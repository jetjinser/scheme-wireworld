all: wireworld.wasm

wireworld.wasm: wireworld.scm
	guild compile-wasm -o wireworld.wasm wireworld.scm

serve: wireworld.wasm
	guile -c '((@ (hoot web-server) serve))'

clean:
	rm wireworld.wasm
